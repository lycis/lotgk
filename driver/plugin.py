import inspect
import pkgutil
import sys

from yapsy.IPlugin import IPlugin

from driver import driver
from driver.area import Zone, Area
from driver.entity import Entity, Template, Command


class DriverPlugin(IPlugin):
    def __init__(self):
        super().__init__()
        self.driver = None  # type: driver.Driver

    def set_driver(self, drv):
        self.driver = drv


class EntityRepository(DriverPlugin):
    def create_entity(self, blueprint: tuple) -> str:
        raise Exception('missing implementation')

    def find_entity(self, id):
        raise Exception('missing implementation')

    def all_entities(self):
        raise Exception('missing implementation')


class PlayerInterface(DriverPlugin):
    def listen(self):
        """
        Starts the interface to listen for player input
        :return: None
        """
        raise Exception('missing implementation')

    def write_to_player(self, id: str, message: str) -> bool:
        """

        :param id: the id of the player
        :param message: the message that is to be routed to the player
        :return: True if the message was routed to the player
        """
        raise Exception('missing implementation')

    def end_player_connection(self, pl):
        raise Exception('missing implementation')


class GameLoader(DriverPlugin):
    def initialise_game_session(self, target_entity):
        """
        Initialises a new game session for the target entity.
        :param target_entity: the entity that is about to represent the player character
        :return: None
        """
        pass


def _fix_module_name(module):
    module_path = module.split(".")[1:]
    if len(module_path) > 0:
        fixed_module_name = ".".join(module_path) + "."
    else:
        fixed_module_name = ""
    return fixed_module_name


def class_location_to_driver_id(c, fixed_module_name):
    return "{}{}".format(fixed_module_name, c[1].__qualname__).lower()


class Bundle(DriverPlugin):
    """
    A bundle is a plugin that adds content to the game by exposing entities, modifications,
    commands, skills or areas.

    All possible types of extensions are automatically detected within the bundle based on their
    parent class (e.g. everything extending Entity is detected as entity)
    """
    def __init__(self):
        super().__init__()
        self._extensions = {}
        self._modifications = {}
        self._gather_exposed_extensions()

    def _gather_exposed_extensions(self):
        modules = []
        for importer, modname, _ in pkgutil.iter_modules(sys.modules[self.__module__].__path__):
            modules.append("{}.{}".format(self.__module__, modname))
        modules.append(self.__module__)

        for module in modules:
            __import__(module)
            self._crawl_module_for_extensions(module)

    def _crawl_module_for_extensions(self, module: str):
        available_cls = inspect.getmembers(sys.modules[module], inspect.isclass)
        fixed_module_name = _fix_module_name(module)
        for c in available_cls:
            cls = c[1]
            if issubclass(cls, Entity) and cls != Entity and not issubclass(cls, Zone) or \
               issubclass(cls, Template) and cls != Template or \
               issubclass(cls, Command) and cls != Command or \
               issubclass(cls, Area) and cls != Area:
                self._extensions[class_location_to_driver_id(c, fixed_module_name)] = c[1]

    @property
    def extensions(self) -> dict:
        return self._extensions
