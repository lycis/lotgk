from driver.entity import Entity
from driver.plugin import EntityRepository


class CoreEntityRepository(EntityRepository):
    def __init__(self):
        super().__init__()
        self.inventory = {}

    def create_entity(self, blueprint: tuple) -> Entity:
        clone = self.driver.clone_entity(blueprint)
        self.inventory[clone.id] = clone
        return clone

    def find_entity(self, id):
        return self.inventory[id]

    def all_entities(self):
        return self.inventory.values()
