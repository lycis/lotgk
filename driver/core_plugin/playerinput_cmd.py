from driver.event import PlayerInput
from driver.plugin import PlayerInterface


class CmdPlayerInput(PlayerInterface):
    def listen(self):
        self.pl = self.driver.new_player()
        print("Player-Id: {}".format(self.pl.id))
        self.pl._interface = self

        while self.driver.is_active():
            plip = input()
            e = PlayerInput(self.pl.id, plip)
            self.driver.post_event(e)

    def write_to_player(self, id: str, message: str) -> bool:
        if id == self.pl.id:
            prompt = self.driver.entities.find_entity(id).get_property("prompt")
            if prompt is None:
                prompt = ">>"
            print("\n{}\n{}".format(message, prompt), end='')
            return True
        return False

    def end_player_connection(self, pl):
        pl.write("Bye!")
        self.driver.shutdown()
