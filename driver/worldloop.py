import time
import traceback
from threading import Thread

from driver.event import RenderPlayerOutput

MS_PER_UPDATE = 16


class WorldLoop(Thread):
    def __init__(self, driver):
        self.driver = driver
        super(WorldLoop, self).__init__()

    def run(self):
        previous = time.time()
        lag = 0.0
        while self.driver.is_active():
            current = time.time()
            elapsed = current - previous
            previous = current
            lag += elapsed

            try:
                self._process_input()
                while lag >= MS_PER_UPDATE:
                    self._update()
                    lag -= MS_PER_UPDATE
                self._render()
            except Exception as e:
                # TODO proper error handling
                print("driver error: {}".format(traceback.format_exc()))

    def _process_input(self):
        event = self.driver.next_event()
        if event is not None:
            self.driver.process_event(event)

    def _update(self):
        self.driver.tick()

    def _render(self):
        self.driver.post_event(RenderPlayerOutput("*"))