from driver.entity import Player, Entity


class Zone(Entity):
    def __init__(self, driver):
        super().__init__(driver)


class Area(Zone):
    def __init__(self, driver, area_id):
        super().__init__(driver)
        self.driver = driver  # type: driver.Driver
        self.scale = 1.0  # type: float
        self._zones = []
        self._active_entities = []
        self.id = area_id
        self.description = ""
        self._map = None

    def set_dimensions(self, width, height):
        if self._map is not None:
            raise Exception("areas must not change size")
        self._map = [[{"zones": [], "entities": []}] * height] * width

    def add_zone(self, coordinates: list, zone: Zone):
        self._zones.append(zone)
        for c in coordinates:
            self._map[c[0]][c[1]]["zones"].append(zone)

    def add_obstacle(self, coordinates: list):
        # TODO place obstacle at coordinates... shomehow
        pass

    def position_entity_at(self, e: Entity, x: int, y: int) -> bool:
        if x < 0 or x > len(self._map) or y < 0 or y > len(self._map[x]):
            raise Exception("cannot place entity ({}) out of area range ({}: x={} y={}".format(
                e.id, self.id, x, y
            ))

        if not self.can_be_positioned_at(e, x, y):
            return False

        if e in self._active_entities:
            loc = e["$location"]
            self._map[loc["x"]][loc["y"]]["entities"].remove(e.id)
        else:
            self._active_entities.append(e)

        e["$location"] = {"area": self.id, "x": x, "y": y}
        self._map[x][y]["entities"].append(e.id)

        return True

    def description_for(self, pl: Player):
        """
        Returns a description of what the player can see at his location.
        :param pl:
        :return:
        """
        if pl not in self._active_entities:
            raise Exception("Player ({}) not in area ({})".format(pl.id, self.id))

        location = pl["$location"]

        return "{areaDesc}\n{zoneDesc}\n{presentEntities}".format(
            areaDesc=self.description,
            zoneDesc=self.zone_descriptions_at(pl, location["x"], location["y"]),
            presentEntities=self.entitiy_descriptions_at(pl, location["x"], location["y"])
        )

    def zone_descriptions_at(self, pl: Player, x, y) -> str:
        """
        Returns the description for all zones that are present at the given coordinates.
        :param x:
        :param y:
        :return:
        """
        # TODO implement
        pass

    def entitiy_descriptions_at(self, pl, x, y):
        """
        Returns a descriptive list of all entities that the player can see from his given location
        :param pl:
        :param x:
        :param y:
        :return:
        """
        # TODO implement
        pass

    def zones_for(self, x: int, y: int) -> list:
        """
        Provides a list of all the zones that this point is part of.

        :param x: x coordinate
        :param y: y corrdinate
        :return: a list of Zone objects that the point is in
        """
        return self._map[x][y]["zones"]

    def entities_at(self, x: int, y: int) -> list:
        return self._map[x][y]["entities"]

    def can_be_positioned_at(self, e, x, y) -> bool:
        """
        This method can be overwritten in order to check if an entity intends to be positioned at the given
        coordinates.
        :param e: entity
        :param x: coordinate
        :param y: coordinate
        :return: when returning False the move will be denied
        """
        return True

    def remove_entity(self, e: Entity):
        """
        Removes this entity from the map.
        :param e: entity
        """
        if e not in self._active_entities:
            return

        self._active_entities.remove(e)
        x = e["$location"]["x"]
        y = e["$location"]["x"]
        self._map[x][y]["entities"].remove(e.id)
        e["$location"] = {"area": None, "x": 0, "y": 0}
