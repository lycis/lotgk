import json
from uuid import uuid5, NAMESPACE_OID
from driver.event import Event, PlayerInput, RenderPlayerOutput, Message, PlayerWantsExit, PlayerExited, EntityMoved

P_TEMPLATE = "templates"


class Template:
    """
    A modification is a reusable template that changes an entity. When it is applied
    to an entity all required changes will be executed on the target entity. Modifications
    can also be reverted and thus undone or be only of temporary nature.
    """

    def __init__(self):
        self._id = ""  # type: str
        self._name = ""  # type: str
        self._type = ""  # type: str
        self._revertible = False  # type: bool

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, t):
        self._type = t

    @property
    def revertible(self):
        return self._revertible

    @revertible.setter
    def revertible(self, r):
        self._revertible = r

    @property
    def id(self):
        return self._id

    def apply(self, entity):
        pass

    def revert(self, entity):
        pass


class Entity:
    def __init__(self, driver):
        self._commands = {}
        self.driver = driver  # type: driver.driver.Driver
        self.id = str(uuid5(NAMESPACE_OID, 'entity'))
        self.event_listener = {}
        self._properties = {}

    def tick(self):
        pass

    def receive_event(self, event: Event):
        if event.__class__ in self.event_listener:
            for f in self.event_listener[event.__class__]:
                f(event)

    def on(self, event_type, fun):
        """
        Execute a function when an event of the given type happens
        :param event_type: class of the event
        :param fun: what to do when the event was received
        """
        if event_type in self.event_listener:
            self.event_listener[event_type].append(fun)
        else:
            self.event_listener[event_type] = [fun]

    def write(self, message: object) -> object:
        """
        Displays a message to the entity.
        :param message: the message to be displayed
        :return: None
        """
        e = Message(self.id, message)
        self.driver.post_event(e)

    def set_property(self, name, value):
        self._properties[name] = value

    def set_properties(self, props: dict):
        for p, v in props.items():
            self.set_property(p, v)

    def get_property(self, name):
        if name not in self._properties:
            return None
        return self._properties[name]

    def save(self, path):
        self.driver.write_file(path, json.dumps({
            "id": self.id,
            "commands": list(self._commands.keys()),
            "properties": self._properties
        }, indent=4, sort_keys=True))

    def command(self, line: str):
        for cmd in self._commands.values():
            if cmd.invoke(self, line):
                return True
        return False

    def add_command(self, cmd_id):
        """
        Adds a new command to the entity.
        :param cmd_id: id of the command
        """

        cmd = self.driver.find_command(cmd_id)
        self._commands[cmd_id] = cmd

    def apply_template(self, mod_id: str):
        """
        Applies the given template to this entity.
        :param mod_id: id of the template to be applied
        """
        if P_TEMPLATE not in self._properties:
            self._properties[P_TEMPLATE] = {}

        mod = self.driver.find_template(mod_id)
        mod.apply(self)
        self._properties[P_TEMPLATE][mod.id] = {"type": mod.type, "name": mod.name}

    def has_template(self, template_id):
        """
        :param template_id: id of the template
        :return: True if a this template is already applied to the entity
        """
        if self._properties is None:
            return False

        return template_id in self.get_property(P_TEMPLATE)

    def __getitem__(self, item):
        return self.get_property(item)

    def __setitem__(self, key, value):
        self.set_property(key, value)

    def move(self, area_id: str, x: int, y: int) -> bool:
        """
        Moves this entity to the given coordinates.
        :param area_id: id of the area
        :param x: x coordinate within the area
        :param y: y coordinate within the area
        :return: True if the move was successful
        """

        area = self.driver.find_area(area_id)
        if area is None:
            raise Exception("Failed to move entity '{}' to undefined area '{}'".format(self.id, area_id))

        if self["$location"] is not None and area_id != self["$location"]["area"]:
            old_area = self.driver.find_area(self["$location"]["area"])
            if old_area is not None:
                old_area.remove_entity(self)

        if not area.position_entity_at(self, x, y):
            return False

        self.driver.post_event(EntityMoved(self.id))
        return True


class Player(Entity):
    def __init__(self, driver):
        super().__init__(driver)
        self._input_route = None
        self._interface = None  # type: plugin.PlayerInterface
        self._message_buffer = []
        self._attach_events()

    def _attach_events(self):
        self.on(PlayerInput, self._dispatch_input)
        self.on(RenderPlayerOutput,
                lambda event: self._render_message_buffer())
        self.on(Message,
                lambda msg: self._message_buffer.append(msg.message))
        self.on(PlayerWantsExit, self._exit_game)

    def _exit_game(self, e):
        self._interface.end_player_connection(self)
        self.driver.post_event(PlayerExited(self.id))

    def route_input(self, fun):
        """
        catches the input of the player and routes it to the given function. The target function will receive the
        player entity as first, and the input as second parameter.

        :param: the function that shall be executed when input is received
        """
        self._input_route = fun

    def _render_message_buffer(self):
        if self._interface is None or len(self._message_buffer) == 0:
            return

        self._interface.write_to_player(self.id, "\n".join(self._message_buffer))
        self._message_buffer = []

    def _dispatch_input(self, event: PlayerInput):
        if self._input_route is not None:
            f = self._input_route
            self._input_route = None
            f(self, event.player_input)
            return

        if self.command(event.player_input):
            return

        self.write("You can't do that!")


class Command:
    """
    This is the base class for all commands.
    """

    def invoke(self, executor: Entity, line: str) -> bool:
        """
        This method is executed when the command is invoked. It will be called
        on every input the player enters. The command has to check for itself if
        the input the player provided shall trigger it.

        When the command was triggered no further commands will be executed.

        :param executor: entity that executes the command
        :param line: the line that has to be checked if it invokes the command
        :return: True when the command was executed or False if the input did not match.
        """
        raise Exception("command invoke() not implemented")
