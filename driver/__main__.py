import os

from driver.driver import Driver

if __name__ == '__main__':
    print("Starting driver.")
    drv = Driver(lib_path=os.path.abspath('.'))
    drv.start()
