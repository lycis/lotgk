from driver.area import Area
from driver.entity import Entity
from unittest.mock import patch


@patch('driver.driver.Driver')
def test_move_entity(MockDriver):
    drv = MockDriver()

    ar = Area(drv, "test.area")
    ar.set_dimensions(3, 3)

    drv.find_area.return_value = ar

    e = Entity(drv)
    e.move('test.area', 0, 0)
    assert(e.id in ar.entities_at(0, 0))
    assert(e["$location"] == {"area": "test.area", "x": 0, "y": 0})

@patch('driver.driver.Driver')
def test_move_entity_different_location_same_area(MockDriver):
    drv = MockDriver()

    ar = Area(drv, "test.area")
    ar.set_dimensions(3, 3)

    drv.find_area.return_value = ar

    e = Entity(drv)
    e.move('test.area', 0, 0)
    assert(e.id in ar.entities_at(0, 0))
    assert(e["$location"] == {"area": "test.area", "x": 0, "y": 0})

    e.move('test.area', 0, 1)
    assert(e.id in ar.entities_at(0, 1))
    assert(e["$location"] == {"area": "test.area", "x": 0, "y": 1})


@patch('driver.driver.Driver')
def test_move_entity_to_different_area(MockDriver):
    drv = MockDriver()

    ar = Area(drv, "test.area")
    ar.set_dimensions(3, 3)

    br = Area(drv, "test.area.2")
    br.set_dimensions(3, 3)

    drv.find_area.side_effect = lambda aid: ar if aid == "test.area" else br

    e = Entity(drv)
    e.move('test.area', 0, 0)
    assert (e.id in ar.entities_at(0, 0))
    assert (e["$location"] == {"area": "test.area", "x": 0, "y": 0})

    e.move('test.area.2', 1, 1)
    assert (e.id in br.entities_at(1, 1))
    assert (e["$location"] == {"area": "test.area.2", "x": 1, "y": 1})
    assert (e.id not in ar.entities_at(0, 0))
