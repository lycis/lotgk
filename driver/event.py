class Event:
    def __init__(self, target):
        """

        :param target: the ID of the targetted entity or entities. In case of multiple targets it has to be a list. None
                       indicates that all entities will receive the event.
        """
        self.target = target

    def is_valid_event_target(self, entity):
        if isinstance(self.target, list):
            if entity.id in self.target:
                return True
            return False
        else:
            return entity.id == self.target


class PlayerInput(Event):
    """
    Signals that a player entered input via a player interface.
    """
    def __init__(self, target, player_input):
        self.player_input = player_input
        super(PlayerInput, self).__init__(target)


class RenderPlayerOutput(Event):
    """
    Signals that there is output that should be rendered to a player.
    """
    pass


class Message(Event):
    """
    Signals that something is about to be displayed for an entity of any kind. The actual display
    of a message will only happen when the driver renders it. Intercepting this event can be used
    to modify what a player gets to read.
    """
    def __init__(self, target, message):
        super().__init__(target)
        self.message = message


class PlayerWantsExit(Event):
    """
    Emitted when a player is about to exit the game.
    """
    pass


class PlayerExited(Event):
    """
    A player has left the game.
    """


class ShutdownRequested(Event):
    """
    Informs the receiver that the game is about to shut down.
    When the game driver shuts down all outstanding events will be processed but no
    new events can be posted!
    """


class AreaLoaded(Event):
    """
    Indicates that a specific area was loaded.
    """
    def __init__(self, target, area_id):
        super().__init__(target)
        self.area_id = area_id


class EntityMoved(Event):
    """
    Informs the receiver that the given entity has moved.
    """