import os
import queue
from importlib import import_module
from os.path import dirname
from pathlib import Path
from threading import Thread

from yapsy.PluginManager import PluginManager

from driver.area import Area
from driver.core_plugin import anchor as core_plugin_anchor
from driver.entity import Player, Entity, Template, Command
from driver.event import Event, ShutdownRequested, AreaLoaded
from driver.plugin import EntityRepository, PlayerInterface, DriverPlugin, GameLoader, Bundle
from driver.worldloop import WorldLoop

PLUGIN_TYPE_GAME_LOADER = "GameLoader"

PLUGIN_TYPE_ENTITY_REPOSITORY = "entity-repository"
PLUGIN_TYPE_PLAYER_INTERFACE = "player-interface"
PLUGIN_TYPE_BUNDLE = "bundle"


class PlayerInterfaceThread(Thread):
    def __init__(self, interface: PlayerInterface):
        super().__init__()
        self.interface = interface
        self.setDaemon(True)

    def run(self) -> None:
        self.interface.listen()


class Driver:
    def __init__(self, lib_path='/mud/'):
        self._loaded_areas = {}
        self._lib_path = lib_path
        self._event_queue = queue.Queue()
        self._id_mapping = {
            "driver.entity.player": Player
        }
        self._active = True

        self._load_plugins()
        self._configure_plugins()

    def start(self):
        """
        Starts the driver world game loop. This function is
        non-blocking and will return immediately when no error occurs.
        """
        self._check_directory_structure()
        self._start_world_loop()

    def _start_world_loop(self):
        self.worldLoop = WorldLoop(self)
        self.worldLoop.start()
        self.worldLoop.join()

    def _load_plugins(self):
        """
        Loads all available plugins and configures the essentials.
        :return: None
        """

        self.pluginManager = PluginManager()
        self.pluginManager.setPluginPlaces([
            "{}/bundles/".format(self._lib_path),
            Path(dirname(core_plugin_anchor.__file__))])
        self.pluginManager.setCategoriesFilter({
            PLUGIN_TYPE_ENTITY_REPOSITORY: EntityRepository,
            PLUGIN_TYPE_PLAYER_INTERFACE: PlayerInterface,
            PLUGIN_TYPE_GAME_LOADER: GameLoader,
            PLUGIN_TYPE_BUNDLE: Bundle
        })
        self.pluginManager.collectPlugins()
        print("Loaded bundles from {}:".format(self._lib_path))
        for pluginInfo in self.pluginManager.getAllPlugins():
            print("* {name} ({type})".format(name=pluginInfo.name, type=pluginInfo.category))
            if isinstance(pluginInfo.plugin_object, DriverPlugin):
                pluginInfo.plugin_object.set_driver(self)

    def _configure_plugins(self):
        # configure entity repository
        self.entities = self.pluginManager.getPluginsOfCategory(PLUGIN_TYPE_ENTITY_REPOSITORY)[0].plugin_object

        # register ids from bundles
        self._configure_bundles()

        # configure player input handler
        self._configure_player_input_handler()

    def _configure_bundles(self):
        for bundleInfo in self.pluginManager.getPluginsOfCategory(PLUGIN_TYPE_BUNDLE):
            bundle = bundleInfo.plugin_object  # type: Bundle
            for id, cls in bundle.extensions.items():
                if id in self._id_mapping:
                    raise Exception("duplicate extension id '{}'".format(id))
                self._id_mapping["{}.{}".format(bundleInfo.name, id)] = cls

    def _configure_player_input_handler(self):
        self.playerInterfaceThread = []
        for interfaceInfo in self.pluginManager.getPluginsOfCategory(PLUGIN_TYPE_PLAYER_INTERFACE):
            t = PlayerInterfaceThread(interfaceInfo.plugin_object)
            t.start()
            self.playerInterfaceThread.append(t)

    def is_active(self):
        if not self._event_queue.empty():
            return True

        return self._active

    def next_event(self):
        try:
            return self._event_queue.get_nowait()
        except queue.Empty:
            return None

    def post_event(self, event: Event):
        if not self._active:
            return

        self._event_queue.put(event, False)

    def process_event(self, event: Event):
        for e in self.entities.all_entities():
            if event.target == "*" or event.is_valid_event_target(e):
                e.receive_event(event)

        # special treatment for shutdown, because we have to stop the driver after every event was
        # notified
        if isinstance(event, ShutdownRequested):
            self._active = False

    def tick(self):
        for e in self.entities.all_entities():
            e.tick()

    def clone_entity(self, entity_id: str):
        entity_id = entity_id.lower()
        if entity_id not in self._id_mapping:
            raise Exception("cloning unregistered id '{}'".format(entity_id))
        cls = self._id_mapping[entity_id]
        if not issubclass(cls, Entity):
            raise Exception("cloning non-entity id '{}'".format(entity_id))
        return cls(self)

    def new_player(self):
        player = self.entities.create_entity("driver.entity.player")
        loader = self.pluginManager.getPluginsOfCategory(PLUGIN_TYPE_GAME_LOADER)[0].plugin_object
        loader.initialise_game_session(player)
        return player

    def write_file(self, path, content):
        path = "{0}/data/{1}".format(self._lib_path, path)

        d = os.path.dirname(path)
        if not os.path.exists(d):
            os.makedirs(d)

        with open(Path(path), 'w') as f:
            f.write(content)

    def _check_directory_structure(self):
        """
        checks if the required file structure exists and creates it if not
        """
        data_path = os.path.dirname("{}/data/".format(self._lib_path))
        if not os.path.exists(data_path):
            os.makedirs(data_path)
            print("Created: {}".format(data_path))

    def find_template(self, template_id: str):
        """
        Returns a template instance for the matching id. If the template
        is now known it will return None
        :param template_id: id of the template
        """
        if template_id not in self._id_mapping:
            return None

        m = self._id_mapping[template_id]

        if not issubclass(m, Template):
            raise Exception("tried to instantiate non-template id '{}".format(template_id))

        return m()

    def find_command(self, cmd_id: str) -> Command:
        """
        Returns a new instance of the command with the given id.
        :param cmd_id: id of the command
        :return: None if not found; otherwise the command instance
        """
        if cmd_id not in self._id_mapping:
            return None

        cls = self._id_mapping[cmd_id]
        if not issubclass(cls, Command):
            raise Exception("tried to instantiate non-command id '{}'".format(cmd_id))

        return cls()

    def find_area(self, area_id: str) -> Area:
        """
        Finds the area of the given name and returns a reference to it.
        If it was not yet loaded it will be.

        :param area_id: id of the area
        :return: reference to the area or None if not found
        """
        if area_id not in self._id_mapping:
            return None

        if area_id in self._loaded_areas:
            return self._loaded_areas[area_id]

        area_cls = self._id_mapping[area_id]
        if not issubclass(area_cls, Area):
            raise Exception("tried to load '{}' as area but it is no area".format(area_id))

        area_instance = area_cls(self)
        self.post_event(AreaLoaded("*", area_id))
        self._loaded_areas[area_id] = area_instance
        return area_instance

    def shutdown(self):
        self.post_event(ShutdownRequested("*"))


def dynamic_import(abs_module_path, class_name):
    module_object = import_module(abs_module_path)
    target_class = getattr(module_object, class_name)
    return target_class
