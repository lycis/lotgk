from driver.area import Area, Zone
from unittest.mock import Mock

from driver.entity import Entity


def test_basic_area():
    a = Area(Mock(), "test.area")
    a.set_dimensions(100, 100)
    assert (len(a._map) == 100)
    assert (len(a._map[0]) == 100)


def test_zones_for_x_y():
    a = Area(Mock(), "test.area")
    a.set_dimensions(100, 100)

    z = Zone(Mock())
    a.add_zone([(0, 0), (0, 1), (1, 0), (1, 1)], z)
    in_zones = a.zones_for(1, 1)

    assert(z in in_zones)


def test_position_entity_at():
    a = Area(Mock(), "test.area")
    a.set_dimensions(3, 3)
    e = Entity(Mock())
    a.position_entity_at(e, 1, 1)
    assert(e.id in a.entities_at(1,1))
    assert(e["$location"] == {"area": "test.area", "x": 1, "y": 1})
