from driver.entity import Player
from driver.plugin import GameLoader
from .new_game import NewGameWizard


class LotgkSessionLoader(GameLoader):
    def initialise_game_session(self, pl: Player):
        super().initialise_game_session(pl)
        self._main_menu_selection(pl)
        pl.apply_template("lotgk-races.interactive")

    def _main_menu_selection(self, pl):
        pl.write("*" * 80)
        pl.write("""
.____           .__                                                 
|    |   _____  |__|______                                          
|    |   \__  \ |  \_  __ \                                         
|    |___ / __ \|  ||  | \/                                         
|_______ (____  /__||__|                                            
        \/    \/                                                    
          _____    __  .__                                          
    _____/ ____\ _/  |_|  |__   ____                                
   /  _ \   __\  \   __\  |  \_/ __ \                               
  (  <_> )  |     |  | |   Y  \  ___/                               
   \____/|__|     |__| |___|  /\___  >                              
                            \/     \/                               
  ________      ___.   .__  .__          ____  __.__                
 /  _____/  ____\_ |__ |  | |__| ____   |    |/ _|__| ____    ____  
/   \  ___ /  _ \| __ \|  | |  |/    \  |      < |  |/    \  / ___\ 
\    \_\  (  <_> ) \_\ \  |_|  |   |  \ |    |  \|  |   |  \/ /_/  >
 \______  /\____/|___  /____/__|___|  / |____|__ \__|___|  /\___  / 
        \/           \/             \/          \/       \//_____/  

""")
        pl.write("*" * 80)
        pl.set_property("prompt", "[Enter to continue]")
        pl.route_input(self._main_menu)

    def _main_menu(self, pl: Player, player_input: str):
        pl.set_property("prompt", None)
        pl.write('''
           .-.---------------------------------.-.
          ((o))                                   )
           \\U/_______          _____         ____/
             |                                  |
             |           Main Menu              |
             |                                  |
             |   1. Start a new game            |
             |                                  |
             |   2. Load game                   |
             |                                  |
             |                                  |
             |____    _______    __  ____    ___|KCK
            /A\\                                  \\
           ((o))                                  )
            '-'----------------------------------'
        ''')
        pl.route_input(self._select_main_menu_action)

    def _select_main_menu_action(self, pl: Player, player_input: str):
        if player_input not in ['1', '2']:
            self._main_menu(pl, '')
            return

        if player_input == '1':
            self._initialise_new_game(pl)
        elif player_input == '2':
            self._initialise_game_from_loadfile(pl)

    def _initialise_new_game(self, pl):
        wiz = NewGameWizard(pl)
        wiz.start()

    def _initialise_game_from_loadfile(self, pl):
        pl.write("Load game!")
