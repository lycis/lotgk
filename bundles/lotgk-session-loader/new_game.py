import random

from driver.entity import Player

races = ["Human", "Elf", "Dwarf", "Orc"]


class NewGameWizard:
    def __init__(self, pl: Player):
        self._player = pl
        self._attribute_allocation = {
            "a_strength": 0,
            "a_intelligence": 0,
            "a_agility": 0,
            "a_constitution": 0,
            "a_alertness": 0,
            "a_willpower": 0,
            "a_charisma": 0
        }
        self._name = ""

    def start(self):
        self._player.write("TODO: display introductory text")  # TODO
        self._select_race()

    def _select_race(self):
        self._player.write("There are {} races in this world. To which one do you belong?".format(len(races)))
        for i in range(0, len(races), 2):
            self._player.write("{}: {:<10} {}: {:<10}".format(i+1, races[i], i+2, races[i+1]))

        self._player.route_input(self._race_selected)

    def _race_selected(self, pl: Player, line: str):
        try:
            nr = int(line)-1
        except ValueError:
            self._select_race()
            return

        if nr < 0 or nr > len(races):
            self._select_race()
            return

        self._player.write("There you are, {}!".format(races[nr]))
        self._player.apply_template("lotgk-races.{}".format(races[nr].lower()))
        self._initialise_attributes()
        self._player.write("Do you want to randomly generate your character statistics? [Y/n]")
        self._player.route_input(self._select_random_generation)
        
    def _select_random_generation(self, pl: Player, line: str):
        line = line.lower()
        if line == "" or line == "y" or line == "yes":
            self._do_random_generation()
        elif line == "n" or line == "no":
            self._do_manual_generation()
        else:
            self._player.write("Do you want to randomly generate a character? [Y/n]")

    def _initialise_attributes(self):
        self._attribute_allocation = {
            "a_strength": 0,
            "a_intelligence": 0,
            "a_agility": 0,
            "a_constitution": 0,
            "a_alertness": 0,
            "a_willpower": 0,
            "a_charisma": 0
        }
        for prop, value in self._attribute_allocation.items():
            if self._player.get_property(prop) is not None:
                self._attribute_allocation[prop] = self._player.get_property(prop)

    def _do_random_generation(self):
        self._initialise_attributes()
        points = 40
        while points > 0:
            random.seed()
            attr = None
            while attr is None:
                attr = list(self._attribute_allocation.keys())[random.randint(0, len(self._attribute_allocation)-1)]
                if self._attribute_allocation[attr] >= 10:
                    attr = None

            self._attribute_allocation[attr] = self._attribute_allocation[attr] + 1
            points = points - 1

        self._player.write("""
           .-.---------------------------------.-.
          ((o))       Attributes                  )
           \\U/_______          _____         ____/
             |                                  |
             |   Strength      : {str:2}             |
             |   Constitution  : {con:2}             |
             |   Intelligence  : {int:2}             |
             |   Willpower     : {wil:2}             |
             |   Charisma      : {cha:2}             |
             |   Agility       : {dex:2}             |
             |   Alertness     : {ale:2}             |
             |____    _______    __  ____    ___|
            /A\\                                  \\
           ((o))                                  )
            '-'----------------------------------'
        """.format(
            str=self._attribute_allocation["a_strength"],
            con=self._attribute_allocation["a_constitution"],
            int=self._attribute_allocation["a_intelligence"],
            wil=self._attribute_allocation["a_willpower"],
            cha=self._attribute_allocation["a_charisma"],
            dex=self._attribute_allocation["a_agility"],
            ale=self._attribute_allocation["a_alertness"]))
        self._player.write("Do you accept these attributes? [Y/n]")
        self._player.route_input(self._check_attribute_reroll)

    def _check_attribute_reroll(self, pl: Player, line: str):
        line = line.lower()
        if line == "" or line == "y" or line == "yes":
            self._enter_name()
            return

        self._player.write("Okay. We are trying it again!")
        self._do_random_generation()

    def _do_manual_generation(self):
        pass

    def _enter_name(self):
        self._player.write("What is your name?")
        self._player.route_input(self._check_name)

    def _check_name(self, pl: Player, line: str):
        line = line.strip()
        if line == "":
            self._player.write("You must have a name!")
            self._enter_name()

        self._name = line
        self._review()

    def _review(self):
        self._player.write(
            """
            +------------------------------------------------------------------+
            |         Character: {name:32}              |
            +------------------------------------------------------------------+
            |                                                                  |
            |   Strength : {str:2}              Constitution: {con:2}                    |
            |   Willpower: {wil:2}              Intelligence: {int:2}                    |
            |   Charisma : {cha:2}              Alertness   : {ale:2}                    |
            |   Agility  : {agi:2}                                                  |
            +------------------------------------------------------------------+
            |      Race: {race:32}                      |
            +------------------------------------------------------------------+
            
            Is this correct? [Y/n]
            """.format(
                name=self._name,
                str=self._attribute_allocation["a_strength"],
                con=self._attribute_allocation["a_constitution"],
                int=self._attribute_allocation["a_intelligence"],
                wil=self._attribute_allocation["a_willpower"],
                cha=self._attribute_allocation["a_charisma"],
                agi=self._attribute_allocation["a_agility"],
                ale=self._attribute_allocation["a_alertness"],
                race=self._player["race"]
            )
        )
        self._player.route_input(self._confirm_character)

    def _confirm_character(self, pl: Player, line : str):
        line = line.lower()
        if line == "n" or line == "no":
            self.start()
            return
        elif len(line) > 0 and not (line == "y" or line == "yes"):
            self._player.write("Please answer with [Y]es or [N]o!")
            self._player.route_input(self._confirm_character)
            return

        pl["name"] = self._name
        for attr, val in self._attribute_allocation.items():
            pl[attr] = val
