from driver.entity import Entity, Template
from driver.plugin import Bundle


class LotgkRacesBundle(Bundle):
    """
    This bundle contains all races that roam the game.
    """
    pass


TPL_TYPE_RACE = "race"
TPL_TYPE_META = "meta"


class Interactive(Template):

    @property
    def name(self):
        return "interactive"

    @property
    def type(self):
        return TPL_TYPE_META

    def apply(self, entity):
        entity.add_command("lotgk-basic-commands.save")
        entity.add_command("lotgk-basic-commands.quit")
        entity.set_property("interactive", True)


class Being(Template):
    @property
    def name(self):
        return "Being"

    @property
    def type(self):
        return TPL_TYPE_RACE

    @property
    def revertible(self):
        return False

    def apply(self, entity):
        super().apply(entity)
        entity.set_properties({
            "a_strength": 0,
            "a_intelligence": 0,
            "a_agility": 0,
            "a_constitution": 0,
            "a_alertness": 0,
            "a_willpower": 0,
            "a_charisma": 0,
            "race": "unknown"
        })


class Human(Being):
    @property
    def name(self):
        return "Human"

    def apply(self, entity):
        super().apply(entity)

        # attribute bonus
        str = entity.get_property("a_strength")
        entity.set_property("a_strength", str+1)
        entity.set_property("race", "Human")
