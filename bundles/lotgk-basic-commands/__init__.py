import time
from driver.entity import Command, Player, Entity
from driver.event import PlayerWantsExit
from driver.plugin import Bundle


class BasicCommands(Bundle):
    pass


class Save(Command):
    """
    Saves a player record.
    """
    def invoke(self, executor: Entity, line: str) -> bool:
        if line != "save" or not isinstance(executor, Player):
            return False

        arg = line[len("save"):]

        description = ""
        if len(arg) > 0:
            description = arg.replace(" ", "_")

        executor.save("/saves/{}_{}".format(str(time.time()), description))
        executor.write("Game saved.")
        return True


class Quit(Command):
    """
    The player leaves the game.
    """
    def invoke(self, executor: Entity, line: str):
        if not isinstance(executor, Player) or not (line == "quit" or line == "exit"):
            return False
        executor.write("Do you want to save? [YES/no]")
        executor.route_input(self._save_if_asked)
        return True

    def _save_if_asked(self, pl: Player, line: str):
        line = line.lower()
        if line == "y" or line == "yes":
            pl.command("save")
        pl.driver.post_event(PlayerWantsExit(pl.id))
        return True
